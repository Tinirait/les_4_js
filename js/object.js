var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

function addStudent(name, point) {
  students.push({ name: name, point: point, show: student });
}

var student = function () {
  console.log('Студент ' +this.name+' набрал ' +this.point+ ' баллов');
}

var students = [];
studentsAndPoints.forEach(function (Value, index, array) {
  if (index%2) {
    addStudent(array[index-1], array[index]);
  }
  });


// Задание 2
addStudent('Николай Фролов', 0);
addStudent('Олег Боровой', 0);

// Задание 3
function addPoint(name, point) {
  students.find(function (student) {
    if (student.name == name) {
      student.point += point;
    }
  });
}

// Добавляем баллы студентам:
addPoint('Ирина Овчинникова', 30);
addPoint('Александр Малов', 30);
addPoint('Николай Фролов', 10);

students.forEach(function (student) {
    student.show();
});
console.log('\n');

// Задание 4
console.log('Задание 4. Студенты, которые набрали 30 и более баллов:');
students.filter(function (student) {
  if (student.point >= 30) {
    student.show();
  }
});
console.log('\n');

// Задание 5
students.forEach(function (student) {
  if (student.point > 0) {
    student.worksAmount = student.point / 10;
  }
});

console.log('Дополнительное задание');

students.findByName = function (name) {
  var object;
  this.find(function (student) {
    if (student.name == name) {
      object = student;
    }
  });
  return object;
};

console.log(students.findByName ('Алексей Петров'));
console.log(students.findByName ('Алексей Рязанцев'));